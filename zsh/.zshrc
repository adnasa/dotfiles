eval "$(starship init zsh)"

# Path to your oh-my-zsh configuration.
ssh-add -A &> /dev/null
ZSH=$HOME/.oh-my-zsh
ZSH_THEME="cloud"
# TYPEWRITTEN_PROMPT_LAYOUT="singleline"
# export TYPEWRITTEN_SYMBOL="❯"

plugins=(git brew autojump yarn)

source $ZSH/oh-my-zsh.sh
setopt menu_complete

export PATH=/usr/local/bin:/usr/local/php5/bin:$PATH
# Add RVM to PATH for scripting
export PATH="$PATH:$HOME/.rvm/bin"
# Add minikube from custom location
export PATH="$PATH:$HOME/workspace/utilities:$PATH"
# kubectl KREW
export PATH="$PATH:$HOME/.krew/bin"


export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# postgres
# export PGDATA='/usr/local/var/postgres'
# export PGHOST=localhost
# alias start-pg='pg_ctl -l $PGDATA/server.log start'
# alias stop-pg='pg_ctl stop -m fast'
# alias show-pg-status='pg_ctl status'
# alias restart-pg='pg_ctl reload'

# Autojump support - takes you to common dir with j [space]
[[ -f /usr/local/etc/autojump.sh ]] && . /usr/local/etc/autojump.sh

# myip
alias myip="ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"

# node
export PATH="$HOME/.node/bin:$PATH"

# docker
alias dmenv_dev='eval "$(docker-machine env dev)"'

# brew
alias brewi='brew install'
alias brews='brew search'

#git
alias git_cp="git branch | grep '^\*' | cut -d' ' -f2 | pbcopy"
alias git_rbm="gsta && gco $(git_main_branch) && ggpull && git checkout - && grb $(git_main_branch)"
alias git_apply="git apply --reject --whitespace=fix"
alias git_rbc="ga . && grbc"
#alias git_ds="git checkout -q main && git for-each-ref refs/heads/ "--format=%(refname:short)" | while read branch; do mergeBase=$(git merge-base main $branch) && [[ $(git cherry main $(git commit-tree $(git rev-parse $branch\^{tree}) -p $mergeBase -m _)) == "-"* ]] && git branch -D $branch; done"


# [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source ~/.oh-my-zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# google cloud
source "/Users/adnasa/workspace/utilities/google-cloud-sdk/path.zsh.inc"
source "/Users/adnasa/workspace/utilities/google-cloud-sdk/completion.zsh.inc"

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh


# JAVA_HOME=$(/usr/libexec/java_home)
## jenv
# export PATH="$HOME/.jenv/bin:$PATH"
# eval "$(jenv init -)"
# LOG_DIR="/var/log/rewe"

# pure theme
#autoload -U promptinit; promptinit
#prompt pure
export PATH="/usr/local/opt/helm@2/bin:$PATH"
