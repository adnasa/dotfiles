" Chaotic setup.

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Plugin 'https://github.com/AndrewRadev/splitjoin.vim'
" Plugin 'https://github.com/vim-scripts/EasyGrep'

Plugin 'https://github.com/Raimondi/delimitMate'
Plugin 'https://github.com/airblade/vim-gitgutter'
Plugin 'https://github.com/bling/vim-airline'
Plugin 'https://github.com/chriskempson/base16-vim'
Plugin 'https://github.com/digitaltoad/vim-jade.git'
Plugin 'https://github.com/docunext/closetag.vim'
Plugin 'https://github.com/editorconfig/editorconfig-vim'
Plugin 'https://github.com/godlygeek/tabular'
Plugin 'https://github.com/groenewege/vim-less'
Plugin 'https://github.com/mattn/emmet-vim'
Plugin 'https://github.com/nathanaelkane/vim-indent-guides'
Plugin 'https://github.com/scrooloose/nerdcommenter'
Plugin 'https://github.com/tpope/vim-abolish'
Plugin 'https://github.com/tpope/vim-fugitive'
Plugin 'https://github.com/tpope/vim-repeat'
Plugin 'https://github.com/vim-scripts/EasyMotion'
Plugin 'https://github.com/vim-scripts/The-NERD-tree'
Plugin 'https://github.com/vim-scripts/matchit.zip'
Plugin 'https://github.com/vim-scripts/surround.vim'
Plugin 'https://github.com/wellle/targets.vim'
Plugin 'https://github.com/pangloss/vim-javascript'
Plugin 'https://github.com/mxw/vim-jsx'
Plugin 'https://github.com/mustache/vim-mustache-handlebars'
Plugin 'https://github.com/MattesGroeger/vim-bookmarks'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Leader mapping ------------------------------------
let mapleader=','

set enc=utf-8
set nobomb
set fileencoding=utf-8
set fileencodings=ucs-bom,utf8,prc

" Search --------------------------------------------
set ignorecase
set smartcase  " case-insensitive on lowecase only, case-sensitive when any uppercase
"set incsearch  " show search matches as you type
set showmatch

" search next/previous -- center in page
nmap n nzz
nmap N Nzz
nmap * *Nzz
nmap # #nzz

" No backups ----------------------------------------
set nobackup
set nowritebackup
set noswapfile

set backspace=2
set cursorline    " highlight the current line the cursor is on
set scrolloff=5

" GUI -----------------------------------------------
set guifont=Fira\ Code:h14
set columns=135
set lines=68
set bg=dark
let &t_Co=256

set guioptions-=T
set number
set numberwidth=5
set showtabline=2
set so=7
set ruler "set ruler "Always show current position
set hlsearch!
set linespace=2
set splitright
set foldenable

" Indentation ---------------------------------------
set autoindent
set shiftwidth=2
set tabstop=2
set expandtab

" Markdown
au BufRead,BufNewFile *.md set filetype=markdown

" css-modules
au BufRead,BufNewFile *.css-modules set filetype=css

" Autocompletion ------------------------------------
set wildmode=list:longest,full

" colorschemes --------------------------------------
colorscheme base16-tomorrow

" Indent guide colorscheme --------------------------
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#272A2A   ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#333333 ctermbg=4

" Nerdtree options ----------------------------------
let NERDTreeWinSize=40
let NERDTreeShowLineNumbers=1
nmap <leader>nt :NERDTreeToggle<cr>
nnoremap <leader><leader>pt :ProjectRootExe NERDTree<CR>
nnoremap <leader><leader>to :NERDTree<CR>
nnoremap <leader><leader>tf :NERDTreeFind<CR>
set selection=exclusive

" Mappings Normal Mode ------------------------------
nmap chls :let @/=""<cr>
nmap <leader>i2 :%s/    /  /g<cr>
nmap <leader>i4 :%s/  /    /g<cr>

" tab mapping ---------------------------------------
nmap <leader>. :tabn<cr>
nmap <leader>m :tabp<cr>

nmap <leader>sl :set list<cr>
nmap <leader>snl :set nolist<cr>
nmap <leader>ww :w <cr>

nmap <S-Enter> o<Esc> " new NEXT line in normal mode
nmap <S-Backspace> O<Esc> " new PREVIOUS line in normal mode

" Abolish
nmap <leader>s. :Subvert /

" clear white space
nmap <leader>ws :%s/\s\+$//<cr>

imap <leader>e <C-y>,

" Nerd commenter
let NERDSpaceDelims=1

" Remove the Windows ^M -----------------------------
noremap <Leader><leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

if has("gui_macvim")
    let macvim_hig_shift_movement = 1
endif

"auto change after saving .vim
if has("autocmd")
    autocmd bufwritepost .vimrc source $MYVIMRC
endif

if exists(":Tabularize")
  nmap <Leader>a= :Tabularize /=<CR>
  vmap <Leader>a= :Tabularize /=<CR>
  nmap <Leader>a: :Tabularize /:\zs<CR>
  vmap <Leader>a: :Tabularize /:\zs<CR>
endif

" vim jsx -------------------------------------------
let g:jsx_ext_required = 0
